//Add contacts, should link to edit button

(function(global) {
    var addContact = { el : $('#page-add') };

    addContact.onSave = function() {
        var name  = addContact.el.find('#fld-name').val(),
            phone = addContact.el.find('#fld-phone').val(),
            home  = addContact.el.find('#fld-home').val(),
            email = addContact.el.find('#fld-email').val();

        global.store.addContact(global.store.ContactInfo(name, phone, email));
        global.pages.contacts.refresh();
    };


    addContact.el.live('pagecreate', function() {
        addContact.el = $(this);
        addContact.el.find('#btn-save-contact').click(addContact.onSave);
    });

    if ( typeof global.pages === 'undefined' ) {
        global.pages = {};
    }

    global.pages.add = addContact;

}(this));