/**
 * Created by gabe on 11/19/17.
 */

 /**
  * This function hides the edit contact text fields & buttons as the page loads
  * */
 $(function() {
    $('.edit-contact-field').hide();
    $('#editDone').hide();
    $('#editCancel').hide();
    $('.information-position').hide();
    $('.information-labels').hide();
    $('#AddDone').hide();
    $('#AddCancel').hide();
});
/**
 * Array that holds all the contact information
 * */
var contacts = [["Gabe Bergstrom","###-###-####","12th ave N Minneapolis, MN 55414","gabe@gmail.com"],
    ["Ana Borjon","###-###-####","23th ave N Minneapolis, MN 55414","ana@gmail.com"],
    ["Ilhan Dahir","###-###-####","99th ave S Minneapolis, MN 55414","ilhan@gmail.com"],
    ["Erik Steinmetz","###-###-####","101th ave S Minneapolis, MN 55234","erik@gmail.com"]];
/**
 * This function is triggered by clicking on a contacts name:
 * It places the information labels and contact information
 */
$('.contact-position').click(function () {
    $('.information-position').show();
    $('.information-labels').show();
})
/**
 * This function for displaying contact information when a user selects a specific contact
 * */
function contactSelected(event, num){
    //gets the values in the innerHTML
    var name = document.getElementById("name")
    var phone = document.getElementById("phone");
    var address = document.getElementById("address");
    var email = document.getElementById("email");

    //adds the items to the screen
    name.innerHTML = contacts[num][0]
    phone.innerHTML = contacts[num][1];
    address.innerHTML = contacts[num][2];
    email.innerHTML = contacts[num][3];

}
/**
 * This function is triggered by clicking the edit button:
 * It places the edit contact text input fields & buttons on the page & hides the edit button
*/
$("#editButton").click(function(){
    $('.edit-contact-field').show();
    $('#editButton').hide();
    $('#editDone').show();
    $('#editCancel').show();
});
/**
 * This function is triggered by clicking in the cancel button:
 * It hides the edit contact text input fields/buttons, shows the edit button,
 * and removes any inputted text into the input fields
*/
$('#editCancel').click(function(){
    $('.edit-contact-field').hide();
    $('#editButton').show();
    $('#editDone').hide();
    $('#editCancel').hide();
    document.getElementById("editName").value = "";
    document.getElementById("editPhone").value = "";
    document.getElementById("editAddress").value = "";
    document.getElementById("editEmail").value = "";
});
/**
 * This function is triggered by clicking the done button:
 * It hides the edit contact text input fields/buttons, shows the edit button,
 * and removes any inputted text into the input fields
 */
$('#editDone').click(function(){
    $('.edit-contact-field').hide();
    $('#editButton').show();
    $('#editDone').hide();
    $('#editCancel').hide();
});
/**
 *
 * @param event
 */
function editData(event){

    //getting the values from the innerHTML
    var name = document.getElementById("name");
    var phone = document.getElementById("phone");
    var hAddress = document.getElementById("address");
    var email = document.getElementById("email");

    //saves the ids to a variable
    var nameEdit =  document.getElementById("editName");
    var phoneEdit = document.getElementById("editPhone");
    var addressEdit =  document.getElementById("editAddress");
    var emailEdit = document.getElementById("editEmail");

    //setting the text in the text field
    if(nameEdit.value != ""){
        name.innerHTML = nameEdit.value;
    }
    hAddress.innerHTML = addressEdit.value;
    phone.innerHTML = phoneEdit.value;
    email.innerHTML = emailEdit.value;

    var index = indexOf(document.getElementById("name").innerHTML);

    try {
    contacts[index][0] = document.getElementById("name").innerHTML;
    }
    catch(err) {
        document.getElementById("name").innerHTML ="Enter the correct contact name";
    }
    contacts[index][1] = document.getElementById("editPhone").value;
    contacts[index][2] = document.getElementById("editAddress").value;
    contacts[index][3] = document.getElementById("editEmail").value;
    document.getElementById("" + index).innerHTML = document.getElementById("name").innerHTML;

    document.getElementById("editName").value = "";
    document.getElementById("editPhone").value = "";
    document.getElementById("editAddress").value = "";
    document.getElementById("editEmail").value = "";

    contactSelected(event,index);
}
/**
 * This function gets the position of the array
 */
function indexOf(name) {
    for (var row = 0; row < contacts.length; row++) {
        if (name == contacts[row][0]) {
            return row;
        }
    }
}
/**
* This function is triggered by clicking the add button:
* The add button will be hidden and the done and cancel buttons will appear.
*/
$('#AddButton').click(function(){
  $('#AddDone').show();
  $('#AddCancel').show();
  $('#AddButton').hide();
});
/**
* This function is triggered by clicking the done button:
* The done and cancel button will be hidden and the add button will appear.
*/
$('#AddDone').click(function(){
  $('#AddDone').hide();
  $('#AddCancel').hide();
  $('#AddButton').show();
});
/**
* This function is triggered by clicking the cancel button:
* The done and cancel button will be hidden and the add button will appear.
*/
$('#AddCancel').click(function(){
  $('#AddDone').hide();
  $('#AddCancel').hide();
  $('#AddButton').show();
});
/**
 * Adds a contact to the user's address book.
 * Requires the username field to not be blank.
 */
function addContacts() {
    // Create a new contact object with the user's input.
    var contact = {
        "name": document.getElementById('name').value,
        "phone": document.getElementById('phone').value,
        "address": document.getElementById('address').value,
        "email": document.getElementById('email').value,
        addContacts:store.addContact(addContact.store.ContactInfo(name, phone, email))
    };

    // Extra check to make sure username isn't empty.
    if (typeof contact.name == "") {
        // Consider it a failed add contact if it was blank.
        onAddFailure("Please enter a name.");
        return;
    }
}
// What to do on a successful add.
function onAddSuccess() {
    log("Add Contact success.");
}

// What to do on a failed add.
function onAddFailure(message) {
    log("Add Contact failure. " + message);
}
