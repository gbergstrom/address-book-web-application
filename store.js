/*Function to store contacts and remove them.
 * This is also in the AddressbookBackEnd file
 */


(function(savecontact) {

    var store = { contacts : [] };

    store.ContactInfo = function(name, phone, address,  email) {
        return { name : name, phone : phone, address: address, email : email }
    };

    store.addContact = function(contact) {
        store.contacts.push(contact);
    };

    store.removeContact = function(contact) {
        for (var i=0; i < store.contacts.length; ++i ) {
            if ( store.contacts[i].phone === contact.phone ) {
                // found it
                store.contacts.splice(i);
                break;
            }
        }
    };


    store.getContacts = function() {
        return store.contacts;
    };

    savecontact.store = store;
}(this));
