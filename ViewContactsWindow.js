/**
 * View contacts window
 *
 * handles contact list page actions
 */

(function (global) {
    var contactsOnPage = { el : $('#page-main') };

    contactsOnPage.initContactList = function(el) {
        var li,
            contacts = global.store.getContacts();

        el.html("");

        for (var i=0; i < contacts.length; ++i ) {
            li = contactsOnPage.createContactListItem(contacts[i]);
            el.append(li);
        }
    };

    contactsOnPage.viewContact = function() {
        var el = $(this),
            contact = el.data('contact'),
            viewer  = $('#page-view');

        viewer.find('span#view-name').text(contact.name);
        viewer.find('span#view-phone').text(contact.phone);
        viewer.find('span#view-home').text(contact.home);
        viewer.find('span#view-email').text(contact.email);

        $.mobile.changePage(viewer);
    };

    contactsOnPage.createContactListItem = function(contact) {
        var li, a;

        li = $(document.createElement('li'));
        a  = $(document.createElement('a'));

        a.attr('href', '#');
        a.text(contact.name);
        a.data('contact', contact);
        a.click(contactsOnPage.viewContact);
        li.append(a);

        return li;
    };

    contactsOnPage.refresh = function() {
        var list_el = contactsOnPage.el.find('#all-contacts');
        contactsOnPage.initContactList(list_el);
        list_el.listview('refresh');
        console.log('refresh');
    };


    $('#page-main').live('pagecreate', function() {
        contactsOnPage.el = $(this);
        var list_el = contactsOnPage.el.find('#all-contacts');

        contactsOnPage.initContactList(list_el);
        list_el.listview();
    });


    if ( typeof global.pages === 'undefined' ) {
        global.pages = {};
    }

    global.pages.contacts = contactsOnPage;
}(this));