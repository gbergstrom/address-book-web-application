This is a web application that functions as an address book. It will have the ability to
display and edit basic information about people ( phone number, address, and email ). 
The page will be driven by javascript functions operating on different parts of the page 
with the layout constructed with html and styled with css. 
